# Web3D 2018 #

Repository containing source code of the [Kitware](https://www.kitware.com/) [Web3D 2018 conference](https://web3d2018.web3d.org/) tutorial on [VTK.js](https://github.com/Kitware/vtk-js).