/* Imports */

const path = require('path');

/* Config */

module.exports = (env, argv) => {

	/* Public path */

	let publicPath = '';

	if(env && env.publicPath)
	{
		publicPath = env.publicPath
	}

	/* Rules */

	let rules = [
		{
			test: require.resolve('./src/Viewers.js'),
			use: [
				{
					loader: 'expose-loader',
					options: 'Kitware',
				},
			],
		},
		{
			test: /\.js$/,
			use: [
				{
					loader: 'babel-loader',
					options: {},
				},
			],
		},
		/*
		{
			test: /\.(glsl|vs|fs)$/,
			loader: 'shader-loader',
		},
		{
			test: /\.worker\.js$/,
			use: [
				{
					loader: 'worker-loader',
					options: {
						inline: true,
						fallback: false,
					},
				},
			],
		},
		*/
	];

	if(argv.mode === 'production')
	{
		rules.push(
			{
				test: /\.js$/,
				use: [
					{
						loader: 'babel-loader',
						options: {},
					},
					{
						loader: 'eslint-loader',
						options: {},
					},
				],
			}
		);
	}

	/* Return */

	return {
		entry: './src/Viewers.js',
		output: {
			path: path.resolve(path.join(__dirname, 'dist')),
			filename: 'Viewers.js',
			publicPath: publicPath,
		},
		resolve: {
			extensions: [
				'.js',
			],
		},
		module: {
			rules: rules,
		},
	};
};