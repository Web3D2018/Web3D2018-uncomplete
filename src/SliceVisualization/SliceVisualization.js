import vtkHttpDataAccessHelper from 'vtk.js/Sources/IO/Core/DataAccessHelper/HttpDataAccessHelper';
import vtkImageMapper from 'vtk.js/Sources/Rendering/Core/ImageMapper';
import vtkImageSlice from 'vtk.js/Sources/Rendering/Core/ImageSlice';
import vtkInteractorStyleImage from 'vtk.js/Sources/Interaction/Style/InteractorStyleImage';
import vtkMath from 'vtk.js/Sources/Common/Core/Math';
import vtkOpenGLRenderWindow from 'vtk.js/Sources/Rendering/OpenGL/RenderWindow';
import vtkRenderWindow from 'vtk.js/Sources/Rendering/Core/RenderWindow';
import vtkRenderWindowInteractor from 'vtk.js/Sources/Rendering/Core/RenderWindowInteractor';
import vtkRenderer from 'vtk.js/Sources/Rendering/Core/Renderer';
import vtkXMLImageDataReader from 'vtk.js/Sources/IO/XML/XMLImageDataReader';

export class SliceVisualization {
	constructor(containerId, fileURL, viewType) {

		/* Attributes */

		this.containerId = null;
		this.container = null;
		this.viewType = null;
		this.renderer = null;
		this.openGLRenderWindow = null;
		this.renderWindow = null;
		this.interactor = null;
		this.interactorStyle = null;
		this.mapper = null;
		this.actor = null;

		/* Initialization */

		this.initializeStructure(containerId);
		this.initializeVisualization();
		this.initializeData(fileURL, viewType);
	}

	initializeStructure(containerId) {

		this.containerId = containerId;

		/* Get container */

		this.container = document.getElementById(this.containerId);

		if(! this.container)
		{
			throw new Error('Container not found.');
		}

		/* Bind updateContainerSize */

		window.addEventListener('resize', () => {
			this.updateSize();
		});
	}

	initializeVisualization() {

		/* Renderer */

		this.renderer = vtkRenderer.newInstance();
		this.renderer.getActiveCamera().setParallelProjection(true);

		/* OpenGLRenderWindow */

		this.openGLRenderWindow = vtkOpenGLRenderWindow.newInstance();
		this.openGLRenderWindow.setContainer(this.container);

		/* RenderWindow */

		this.renderWindow = vtkRenderWindow.newInstance();
		this.renderWindow.addRenderer(this.renderer);
		this.renderWindow.addView(this.openGLRenderWindow);

		/* Interactor */

		this.interactor = vtkRenderWindowInteractor.newInstance();
		this.interactor.setView(this.openGLRenderWindow);
		this.interactor.initialize();
		this.interactor.bindEvents(this.container);

		/* InteractorStyle */

		this.interactorStyle = vtkInteractorStyleImage.newInstance();

		this.renderWindow.getInteractor().setInteractorStyle(this.interactorStyle);

		/* Mapper */

		// 1.1 Create a new imageMapper mapper

		/* Actor */

		// 1.2 Create a new imageSlice actor
		// 1.3 Set actor mapper

		/* Initialize size */

		this.updateSize();
	}

	initializeData(fileURL, viewType) {

		this.viewType = viewType;

		/* Fetch file */

		vtkHttpDataAccessHelper
			.fetchBinary(fileURL)
			.then((content) => {

				/* Parse file */

				const reader = vtkXMLImageDataReader.newInstance();

				reader.parseAsArrayBuffer(content);

				/* Update */

				const data = reader.getOutputData();

				this.mapper.setInputData(data);

				this.renderer.addVolume(this.actor);

				this.updateCamera();
			});
	}

	updateCamera() {
		if(this.mapper.getInputData())
		{
			/* Compute best cameraParameters */

			this.renderer.resetCamera();

			const camera = this.renderer.getActiveCamera();
			const position = camera.getPosition();
			const focalPoint = camera.getFocalPoint();
			const distance = Math.sqrt(vtkMath.distance2BetweenPoints(position, focalPoint));

			let viewUp = [];
			let newPosition = focalPoint.slice(0);
			let radius = 1;
			const bounds = this.mapper.getInputData().getBounds();

			if(this.viewType === 'X')
			{
				viewUp = [0, 1, 0];
				newPosition[0] += distance;
				radius = Math.max(bounds[3] - bounds[2], bounds[5] - bounds[4]);
			}
			else if(this.viewType === 'Y')
			{
				viewUp = [-1, 0, 0];
				newPosition[1] += distance;
				radius = Math.max(bounds[1] - bounds[0], bounds[5] - bounds[4]);
			}
			else if(this.viewType === 'Z')
			{
				viewUp = [0, 1, 0];
				newPosition[2] += distance;
				radius = Math.max(bounds[1] - bounds[0], bounds[3] - bounds[2]);
			}

			camera.setViewUp(...viewUp);
			camera.setPosition(...newPosition);
			camera.setParallelScale(radius * 0.5);

			this.renderer.resetCameraClippingRange(bounds);

			this.setSlicePercentage(0);
		}
	}

	setSlicePercentage(slicePercentage) {
		const extent = this.mapper.getInputData().getExtent();

		// 2.1 Get data extent

		// 2.2 Set mapper slice index

		// 2.3 Force renderWindow rendering
	}

	updateSize() {
		if(this.container && this.openGLRenderWindow && this.renderWindow)
		{
			const containerDimensions = this.container.getBoundingClientRect();

			if(containerDimensions)
			{
				this.openGLRenderWindow.setSize(containerDimensions.width, containerDimensions.height);

				this.renderWindow.render();
			}
		}
	}
}