import vtkActor from 'vtk.js/Sources/Rendering/Core/Actor';
import vtkCutter from 'vtk.js/Sources/Filters/Core/Cutter';
import vtkHttpDataAccessHelper from 'vtk.js/Sources/IO/Core/DataAccessHelper/HttpDataAccessHelper';
import vtkInteractorStyleTrackballCamera from 'vtk.js/Sources/Interaction/Style/InteractorStyleTrackballCamera';
import vtkMapper from 'vtk.js/Sources/Rendering/Core/Mapper';
import vtkOBJReader from 'vtk.js/Sources/IO/Misc/OBJReader';
import vtkOpenGLRenderWindow from 'vtk.js/Sources/Rendering/OpenGL/RenderWindow';
import vtkPlane from 'vtk.js/Sources/Common/DataModel/Plane';
import vtkRenderWindow from 'vtk.js/Sources/Rendering/Core/RenderWindow';
import vtkRenderWindowInteractor from 'vtk.js/Sources/Rendering/Core/RenderWindowInteractor';
import vtkRenderer from 'vtk.js/Sources/Rendering/Core/Renderer';

export class CutterFilterVisualization {
	constructor(containerId, fileURL) {

		/* Attributes */

		this.containerId = null;
		this.container = null;
		this.renderer = null;
		this.openGLRenderWindow = null;
		this.renderWindow = null;
		this.interactor = null;
		this.interactorStyle = null;
		this.mapper = null;
		this.actor = null;
		this.data = null;
		this.cutter = null;
		this.plane = null;

		/* Initialization */

		this.initializeStructure(containerId);
		this.initializeVisualization();
		this.initializeData(fileURL);
	}

	initializeStructure(containerId) {

		this.containerId = containerId;

		/* Get container */

		this.container = document.getElementById(this.containerId);

		if(! this.container)
		{
			throw new Error('Container not found.');
		}

		/* Bind updateContainerSize */

		window.addEventListener('resize', () => {
			this.updateSize();
		});
	}

	initializeVisualization() {

		/* Renderer */

		this.renderer = vtkRenderer.newInstance();
		this.renderer.getActiveCamera().setParallelProjection(true);

		/* OpenGLRenderWindow */

		this.openGLRenderWindow = vtkOpenGLRenderWindow.newInstance();
		this.openGLRenderWindow.setContainer(this.container);

		/* RenderWindow */

		this.renderWindow = vtkRenderWindow.newInstance();
		this.renderWindow.addRenderer(this.renderer);
		this.renderWindow.addView(this.openGLRenderWindow);

		/* Interactor */

		this.interactor = vtkRenderWindowInteractor.newInstance();
		this.interactor.setView(this.openGLRenderWindow);
		this.interactor.initialize();
		this.interactor.bindEvents(this.container);

		/* InteractorStyle */

		this.interactorStyle = vtkInteractorStyleTrackballCamera.newInstance();

		this.renderWindow.getInteractor().setInteractorStyle(this.interactorStyle);

		/* Mapper */

		this.mapper = vtkMapper.newInstance();

		/* Actor */

		this.actor = vtkActor.newInstance();
		this.actor.setMapper(this.mapper);

		/* Cutter */

		// 1.1.1 Create new plane
		// 1.1.2 Set plane normal
		// 1.1.3 Set plane origin

		// 1.2.1 Create new cutter
		// 1.2.2 Set cutter cutFunction with plane

		/* Initialize size */

		this.updateSize();
	}

	initializeData(fileURL) {

		/* Fetch file */

		vtkHttpDataAccessHelper
			.fetchText({}, fileURL)
			.then((content) => {

				/* Parse file */

				const reader = vtkOBJReader.newInstance();

				reader.parseAsText(content);

				/* Update */

				this.data = reader.getOutputData();

				// 2.1 Set cutter data

				// 2.2 Set connect mapper input with cutter output

				this.renderer.addActor(this.actor);

				this.renderer.resetCamera();

				this.renderWindow.render();
			});
	}

	setCutterPercentage(cutterPercentage) {
		if(this.data && this.plane)
		{
			// 3.1 Get data bounds

			// 3.2 Set plane origin

			this.renderer.resetCamera();
			this.renderWindow.render();
		}
	}

	updateSize() {
		if(this.container && this.openGLRenderWindow && this.renderWindow)
		{
			const containerDimensions = this.container.getBoundingClientRect();

			if(containerDimensions)
			{
				this.openGLRenderWindow.setSize(containerDimensions.width, containerDimensions.height);

				this.renderWindow.render();
			}
		}
	}
}