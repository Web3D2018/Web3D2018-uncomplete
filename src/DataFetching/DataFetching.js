import vtkActor from 'vtk.js/Sources/Rendering/Core/Actor';
import vtkHttpDataAccessHelper from 'vtk.js/Sources/IO/Core/DataAccessHelper/HttpDataAccessHelper';
import vtkInteractorStyleTrackballCamera from 'vtk.js/Sources/Interaction/Style/InteractorStyleTrackballCamera';
import vtkMapper from 'vtk.js/Sources/Rendering/Core/Mapper';
import vtkOBJReader from 'vtk.js/Sources/IO/Misc/OBJReader';
import vtkOpenGLRenderWindow from 'vtk.js/Sources/Rendering/OpenGL/RenderWindow';
import vtkRenderWindow from 'vtk.js/Sources/Rendering/Core/RenderWindow';
import vtkRenderWindowInteractor from 'vtk.js/Sources/Rendering/Core/RenderWindowInteractor';
import vtkRenderer from 'vtk.js/Sources/Rendering/Core/Renderer';

export class DataFetching {
	constructor(containerId, fileURL) {

		/* Attributes */

		this.containerId = null;
		this.container = null;
		this.renderer = null;
		this.openGLRenderWindow = null;
		this.renderWindow = null;
		this.interactor = null;
		this.interactorStyle = null;
		this.mapper = null;
		this.actor = null;

		/* Initialization */

		this.initializeStructure(containerId);
		this.initializeVisualization();
		this.initializeData(fileURL);
	}

	initializeStructure(containerId) {

		this.containerId = containerId;

		/* Get container */

		this.container = document.getElementById(this.containerId);

		if(! this.container)
		{
			throw new Error('Container not found.');
		}

		/* Bind updateContainerSize */

		window.addEventListener('resize', () => {
			this.updateSize();
		});
	}

	initializeVisualization() {

		/* Renderer */

		this.renderer = vtkRenderer.newInstance();
		this.renderer.getActiveCamera().setParallelProjection(true);

		/* OpenGLRenderWindow */

		this.openGLRenderWindow = vtkOpenGLRenderWindow.newInstance();
		this.openGLRenderWindow.setContainer(this.container);

		/* RenderWindow */

		this.renderWindow = vtkRenderWindow.newInstance();
		this.renderWindow.addRenderer(this.renderer);
		this.renderWindow.addView(this.openGLRenderWindow);

		/* Interactor */

		this.interactor = vtkRenderWindowInteractor.newInstance();
		this.interactor.setView(this.openGLRenderWindow);
		this.interactor.initialize();
		this.interactor.bindEvents(this.container);

		/* InteractorStyle */

		this.interactorStyle = vtkInteractorStyleTrackballCamera.newInstance();

		this.renderWindow.getInteractor().setInteractorStyle(this.interactorStyle);

		/* Mapper */

		this.mapper = vtkMapper.newInstance();

		/* Actor */

		this.actor = vtkActor.newInstance();
		this.actor.setMapper(this.mapper);

		/* Initialize size */

		this.updateSize();
	}

	initializeData(fileURL) {

		/* Fetch file */

		// 1 Use a httpDataAccessHelper to get the content of the fileURL (as text)

		// 2.1.1 Create a new OBJReader
		// 2.1.2 Use the reader to parse the content returned by the httpDataAccessHelper
		// 2.2 Set mapper input to reader output
		// 2.3.1 Add actor to the renderer
		// 2.3.2 Reset renderer camera
		// 2.4 Force renderWindow rendering
	}

	updateSize() {
		if(this.container && this.openGLRenderWindow && this.renderWindow)
		{
			const containerDimensions = this.container.getBoundingClientRect();

			if(containerDimensions)
			{
				this.openGLRenderWindow.setSize(containerDimensions.width, containerDimensions.height);

				this.renderWindow.render();
			}
		}
	}
}