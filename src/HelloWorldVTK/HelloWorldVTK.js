import vtkActor from 'vtk.js/Sources/Rendering/Core/Actor';
import vtkConeSource from 'vtk.js/Sources/Filters/Sources/ConeSource';
import vtkInteractorStyleTrackballCamera from 'vtk.js/Sources/Interaction/Style/InteractorStyleTrackballCamera';
import vtkMapper from 'vtk.js/Sources/Rendering/Core/Mapper';
import vtkOpenGLRenderWindow from 'vtk.js/Sources/Rendering/OpenGL/RenderWindow';
import vtkRenderWindow from 'vtk.js/Sources/Rendering/Core/RenderWindow';
import vtkRenderWindowInteractor from 'vtk.js/Sources/Rendering/Core/RenderWindowInteractor';
import vtkRenderer from 'vtk.js/Sources/Rendering/Core/Renderer';

export class HelloWorldVTK {
	constructor(containerId) {

		/* Attributes */

		this.containerId = null;
		this.container = null;
		this.renderer = null;
		this.openGLRenderWindow = null;
		this.renderWindow = null;
		this.interactor = null;
		this.interactorStyle = null;
		this.mapper = null;
		this.actor = null;

		/* Initialization */

		this.initializeStructure(containerId);
		this.initializeVisualization();
		this.initializeData();
	}

	initializeStructure(containerId) {

		this.containerId = containerId;

		/* Get container */

		this.container = document.getElementById(this.containerId);

		if(! this.container)
		{
			throw new Error('Container not found.');
		}

		/* Bind updateContainerSize */

		window.addEventListener('resize', () => {
			this.updateSize();
		});
	}

	initializeVisualization() {

		/* Renderer */

		// 1.1.1 Create a new renderer
		// 1.1.2 Set renderer projection to parallel

		/* OpenGLRenderWindow */

		// 1.2.1 Create a new openGLRenderWindow
		// 1.2.2 Set openGLRenderWindow container

		/* RenderWindow */

		// 1.3.1 Create a new renderWindow
		// 1.3.2 Add renderer to renderWindow
		// 1.3.3 Add view (openGLRenderWindow) to renderWindow

		/* Interactor */

		// 1.4.1 Create new renderWindowInteractor
		// 1.4.2 Add view (openGLRenderWindow) to renderWindowInteractor
		// 1.4.3 Initialize interactor
		// 1.4.4 Bind container events to interactor

		/* InteractorStyle */

		// 1.5 Create a new interactorStyleTrackballCamera

		// 1.6 Set interactor interactorStyle

		/* Mapper */

		// 1.7 Create new mapper

		/* Actor */

		// 1.8.1 Create new actor
		// 1.8.2 Set mapper actor

		/* Initialize size */

		this.updateSize();
	}

	initializeData() {

		const coneFilter = vtkConeSource.newInstance({
			height: 1,
		});

		// this.mapper.setInputConnection(coneFilter.getOutputPort());

		// 2.1.1 Add actor to renderer
		// 2.1.2 Reset renderer camera

		// 2.2 Force renderWindow rendering
	}

	updateSize() {
		if(this.container && this.openGLRenderWindow && this.renderWindow)
		{
			const containerDimensions = this.container.getBoundingClientRect();

			if(containerDimensions)
			{
				this.openGLRenderWindow.setSize(containerDimensions.width, containerDimensions.height);

				this.renderWindow.render();
			}
		}
	}
}