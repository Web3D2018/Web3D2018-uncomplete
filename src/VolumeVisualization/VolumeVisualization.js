import vtkColorTransferFunction from 'vtk.js/Sources/Rendering/Core/ColorTransferFunction';
import vtkHttpDataAccessHelper from 'vtk.js/Sources/IO/Core/DataAccessHelper/HttpDataAccessHelper';
import vtkInteractorStyleTrackballCamera from 'vtk.js/Sources/Interaction/Style/InteractorStyleTrackballCamera';
import vtkOpenGLRenderWindow from 'vtk.js/Sources/Rendering/OpenGL/RenderWindow';
import vtkPiecewiseFunction from 'vtk.js/Sources/Common/DataModel/PiecewiseFunction';
import vtkRenderWindow from 'vtk.js/Sources/Rendering/Core/RenderWindow';
import vtkRenderWindowInteractor from 'vtk.js/Sources/Rendering/Core/RenderWindowInteractor';
import vtkRenderer from 'vtk.js/Sources/Rendering/Core/Renderer';
import vtkVolumeMapper from 'vtk.js/Sources/Rendering/Core/VolumeMapper';
import vtkVolume from 'vtk.js/Sources/Rendering/Core/Volume';
import vtkXMLImageDataReader from 'vtk.js/Sources/IO/XML/XMLImageDataReader';

export class VolumeVisualization {
	constructor(containerId, fileURL) {

		/* Attributes */

		this.containerId = null;
		this.container = null;
		this.renderer = null;
		this.openGLRenderWindow = null;
		this.renderWindow = null;
		this.interactor = null;
		this.interactorStyle = null;
		this.mapper = null;
		this.actor = null;
		this.colorTransferFunction = null;
		this.opacityTransferFunction = null;

		/* Initialization */

		this.initializeStructure(containerId);
		this.initializeVisualization();
		this.initializeData(fileURL);
	}

	initializeStructure(containerId) {

		this.containerId = containerId;

		/* Get container */

		this.container = document.getElementById(this.containerId);

		if(! this.container)
		{
			throw new Error('Container not found.');
		}

		/* Bind updateContainerSize */

		window.addEventListener('resize', () => {
			this.updateSize();
		});
	}

	initializeVisualization() {

		/* Renderer */

		this.renderer = vtkRenderer.newInstance();

		/* OpenGLRenderWindow */

		this.openGLRenderWindow = vtkOpenGLRenderWindow.newInstance();
		this.openGLRenderWindow.setContainer(this.container);

		/* RenderWindow */

		this.renderWindow = vtkRenderWindow.newInstance();
		this.renderWindow.addRenderer(this.renderer);
		this.renderWindow.addView(this.openGLRenderWindow);

		/* Interactor */

		this.interactor = vtkRenderWindowInteractor.newInstance();
		this.interactor.setView(this.openGLRenderWindow);
		this.interactor.initialize();
		this.interactor.bindEvents(this.container);

		/* InteractorStyle */

		this.interactorStyle = vtkInteractorStyleTrackballCamera.newInstance();

		this.renderWindow.getInteractor().setInteractorStyle(this.interactorStyle);

		/* Mapper */

		// 1.1 Create a new volumeMapper mapper

		/* Actor */

		// 1.2 Create a new volume actor
		// 1.3 Set actor mapper

		/* Visualization parameters */

		/** Create ColorTransferFunction **/

		// 4.1.1 Create a new colorTransferFunction
		// 4.1.2 Set the colorTransferFunction of the actor

		/** Create OpacityTransferFunction **/

		// 4.2.1 Create a new piecewiseFunction opacityTransferFunction
		// 4.2.2 Set the opacityTransferFunction of the actor

		/** Others **/

		/*
		this.actor.getProperty().setScalarOpacityUnitDistance(0, 0.5);
		this.actor.getProperty().setInterpolationTypeToLinear();
		this.actor.getProperty().setUseGradientOpacity(0, true);
		this.actor.getProperty().setGradientOpacityMinimumValue(0, 15);
		this.actor.getProperty().setGradientOpacityMaximumValue(0, 100);
		this.actor.getProperty().setGradientOpacityMinimumOpacity(0, 0.0);
		this.actor.getProperty().setGradientOpacityMaximumOpacity(0, 1.0);
		this.actor.getProperty().setShade(true);
		this.actor.getProperty().setAmbient(0.2);
		this.actor.getProperty().setDiffuse(0.7);
		this.actor.getProperty().setSpecular(0.3);
		this.actor.getProperty().setSpecularPower(8.0);
		*/

		/* Initialize size */

		this.updateSize();
	}

	initializeData(fileURL) {

		/* Fetch file */

		// 2 Use a httpDataAccessHelper to get the content of the fileURL (as binary)

		// 3.1.1 Create a new XMLImageDataReader
		// 3.1.2 Use the reader to parse the content returned by the httpDataAccessHelper
		// 3.2 Set mapper input to reader output
		// 3.3.1 Add actor to the renderer
		// 3.3.2 Reset renderer camera
		// 3.4 Force renderWindow rendering

		// 4.3.1 Get data range
		// 4.3.2 Set colorTransferFunction color points using data range
		// 4.3.3 Set opacityTransferFunction opacity points using data range
	}

	updateSize() {
		if(this.container && this.openGLRenderWindow && this.renderWindow)
		{
			const containerDimensions = this.container.getBoundingClientRect();

			if(containerDimensions)
			{
				this.openGLRenderWindow.setSize(containerDimensions.width, containerDimensions.height);

				this.renderWindow.render();
			}
		}
	}
}