import vtkActor from 'vtk.js/Sources/Rendering/Core/Actor';
import vtkInteractorStyleTrackballCamera from 'vtk.js/Sources/Interaction/Style/InteractorStyleTrackballCamera';
import vtkMapper from 'vtk.js/Sources/Rendering/Core/Mapper';
import vtkOpenGLRenderWindow from 'vtk.js/Sources/Rendering/OpenGL/RenderWindow';
import vtkRenderWindow from 'vtk.js/Sources/Rendering/Core/RenderWindow';
import vtkRenderWindowInteractor from 'vtk.js/Sources/Rendering/Core/RenderWindowInteractor';
import vtkRenderer from 'vtk.js/Sources/Rendering/Core/Renderer';

import vtkExtendSphereSource from './ExtendSphereSource';

export class ExtendClass {
	constructor(containerId) {

		/* Attributes */

		this.containerId = null;
		this.container = null;
		this.renderer = null;
		this.openGLRenderWindow = null;
		this.renderWindow = null;
		this.interactor = null;
		this.interactorStyle = null
		this.mapper = null;
		this.actor = null;

		this.extendSphereSource = null;

		/* Initialization */

		this.initializeStructure(containerId);
		this.initializeVisualization();
	}

	initializeStructure(containerId) {

		this.containerId = containerId;

		/* Get container */

		this.container = document.getElementById(this.containerId);

		if(! this.container)
		{
			throw new Error('Container not found.');
		}

		/* Bind updateContainerSize */

		window.addEventListener('resize', () => {
			this.updateSize();
		});
	}

	initializeVisualization() {

		/* Renderer */

		this.renderer = vtkRenderer.newInstance();

		/* OpenGLRenderWindow */

		this.openGLRenderWindow = vtkOpenGLRenderWindow.newInstance();
		this.openGLRenderWindow.setContainer(this.container);

		/* RenderWindow */

		this.renderWindow = vtkRenderWindow.newInstance();
		this.renderWindow.addRenderer(this.renderer);
		this.renderWindow.addView(this.openGLRenderWindow);

		/* Interactor */

		this.interactor = vtkRenderWindowInteractor.newInstance();
		this.interactor.setView(this.openGLRenderWindow);
		this.interactor.initialize();
		this.interactor.bindEvents(this.container);

		/* InteractorStyle */

		this.interactorStyle = vtkInteractorStyleTrackballCamera.newInstance();

		this.renderWindow.getInteractor().setInteractorStyle(this.interactorStyle);

		/* CustomSphereSource */

		this.extendSphereSource = vtkExtendSphereSource.newInstance({
			thetaResolution: 50,
			phiResolution: 50,
		});

		/* Mapper */

		this.mapper = vtkMapper.newInstance();
		this.mapper.setInputConnection(this.extendSphereSource.getOutputPort());

		/* Actor */

		this.actor = vtkActor.newInstance();
		this.actor.setMapper(this.mapper);
		this.actor.getProperty().setEdgeVisibility(true);

		this.renderer.addActor(this.actor);

		this.renderer.resetCamera();
		this.renderWindow.render();

		/* Initialize size */

		this.updateSize();
	}

	updateSize() {
		if(this.container && this.openGLRenderWindow && this.renderWindow)
		{
			const containerDimensions = this.container.getBoundingClientRect();

			if(containerDimensions)
			{
				this.openGLRenderWindow.setSize(containerDimensions.width, containerDimensions.height);

				this.renderWindow.render();
			}
		}
	}

	setRadius(radius) {
		if(this.extendSphereSource)
		{
			this.extendSphereSource.setRadius2(radius);

			this.renderer.resetCamera();
			this.renderWindow.render();
		}
	}
}
