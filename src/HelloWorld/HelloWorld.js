export class HelloWorld {
	constructor(containerId) {
		const container = document.getElementById(containerId);

		if(container)
		{
			container.innerHTML = 'HelloWorld';
		}
		else
		{
			throw new Error('Container not found.');
		}
	}
}